class TemplateProvider:
    """
    This class provides a CloudFormation template in one of 2 forms:
    - Template body as a String (its length must not exceed 51,200 bytes)
    - Link to a S3 bucket which contains template body (bucket size must not exceed 460,800 bytes).
    See https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cloudformation-limits.html
    """

    def __init__(self):
        pass

    def has_body(self):
        """
        :return: True if this provider contains a template body. False otherwise.
        """
        raise Exception("Not implemented")

    def get_body(self):
        raise Exception("Not implemented")

    def get_bucket_url(self):
        raise Exception("Not implemented")


class BodyTemplateProvider(TemplateProvider):
    """
    Provide the body of a CloudFormation template
    """

    def __init__(self, body):
        TemplateProvider.__init__(self)
        self._body = body

    def has_body(self):
        return True

    def get_body(self):
        return self._body


class BucketTemplateProvider(TemplateProvider):
    """
    Provide link to an S3 bucket of a CloudFormation template
    """

    def __init__(self, bucket_url):
        TemplateProvider.__init__(self)
        self._bucket_url = bucket_url

    def has_body(self):
        return False

    def get_bucket_url(self):
        return self._bucket_url
