import logging
import os
import time
import requests

from common import Utils
from common.E3 import e3
from confluence.SetupHelper import SetupHelper
from confluence.UserCreatorHelper import UserCreatorHelper
from provisioning.Template import Template
from provisioning.confluence.ConfluenceSetupWizard import ConfluenceInstance


class ConfluenceDataCenter(Template):
    @staticmethod
    def _get_total_created_user(instance_properties):
        if 'confluence.number.of.users' not in instance_properties:
            return 0

        return 0 if instance_properties['confluence.number.of.users'] is None \
            else int(instance_properties['confluence.number.of.users'])

    @staticmethod
    def _get_cluster_size(instance_properties):
        if 'confluence.number.of.instances' not in instance_properties:
            return 1

        return 1 if instance_properties['confluence.number.of.instances'] is None \
            else int(instance_properties['confluence.number.of.instances'])

    @staticmethod
    def _get_created_user_threshold(instance_properties):
        if 'confluence.min.users.to.run.experiment' not in instance_properties:
            return 0

        return 0 if instance_properties['confluence.min.users.to.run.experiment'] is None \
            else int(instance_properties['confluence.min.users.to.run.experiment'])

    def __init__(self, aws, e3_properties, template="ConfluenceDataCenter"):
        self._log = logging.getLogger('provision')

        stack_config = {
            "StackName": "",
            "Template": template,
            "CloudFormation": {
                'AssociatePublicIpAddress': str(e3_properties['public']).lower(),
                'CatalinaOpts': '-Xms4096m -Xmx4096m -XX:+UseG1GC -Dcom.sun.management.jmxremote.port=3333 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Dconfluence.hazelcast.jmx.enable=true -Dconfluence.hibernate.jmx.enable=true',
                'ConfluenceVersion': '6.1.0-m22',
                'ClusterNodeInstanceType': 'c3.xlarge',
                "CidrBlock": '0.0.0.0/0',
                'DBMasterUserPassword': 'conf3last1c',
                'DBPassword': 'confluence',
                'ExternalSubnets': self.get_subnets,
                'InternalSubnets': self.get_subnets,
                'KeyName': self.get_stack_name,
                'StartCollectd': 'true',
                'VPC': self.get_vpc,
            },
            "Orchestration": {
                "CollectdConfig": "confluence-collectd.conf",
                "StackNamePrefix": "confluence-data-center"
            },
            "Output": {
                "ClusterNodes": []
            }
        }

        if "version" in e3_properties:
            stack_config["CloudFormation"]["ConfluenceVersion"] = e3_properties["version"]

        if "parameters" not in e3_properties:
            stack_config["CloudFormation"]["parameters"]["ClusterNodeMin"] = "1"
            stack_config["CloudFormation"]["parameters"]["ClusterNodeMax"] = "1"

        Template.__init__(self, aws, e3_properties, stack_config)

    def after_provision(self):
        stack_name = self._stack_config["StackName"]
        confluence_bl_url = self.get_stack_output(stack_name, 'URL')
        asg_name = self.get_stack_output(stack_name, 'ClusterNodeGroup')
        ssg_name = self.get_stack_output(stack_name, 'SynchronyClusterNodeGroup')
        self.wait_confluence_start(stack_name)
        self.write_provisioning_metadata()
        # Going through setup wizard
        self._log.info("Confluence stack is started. Preparing to run setup wizard.")
        time.sleep(20)

        confluence_instance = ConfluenceInstance(
            base_url=confluence_bl_url,
            properties=self._e3_properties['properties'])
        self._setup_confluence(confluence_instance=confluence_instance)
        self._log.info("Confluence setup finished. Starting Synchrony.")
        self.start_synchrony(stack_name)
        self._log.info("Synchrony started successfully - Confluence stack is fully started.")

        total_created_user = self._get_total_created_user(confluence_instance.properties)
        if total_created_user > 0:
            self._log.info("Creating users for load testing")
            user_creator_helper = UserCreatorHelper(total_created_user, confluence_bl_url)
            user_creator_helper.prepare_test_users()
            user_created_threshold = self._get_created_user_threshold(confluence_instance.properties)
            if user_created_threshold > 0 \
                    and user_created_threshold > user_creator_helper.get_num_success_created_user():
                self._log.error("Could not create enough users to start load test")
                raise Exception(
                    'Could not create enough users to start load test. Threshold [%d]'
                    % user_created_threshold)

        self._stack_config["Output"]['ClusterNodes'] = self.ssh_connection_strings_for_auto_scaling_group(asg_name)
        self._stack_config["Output"]['SynchronyClusterNodes'] = self.ssh_connection_strings_for_auto_scaling_group(ssg_name)
        self._stack_config["Output"]['ClusterNodeGroup'] = asg_name
        self._stack_config["Output"]['SynchronyClusterNodeGroup'] = ssg_name
        self._stack_config["Output"]['NetworkStack'] = self._e3_properties['network']
        self._stack_config["Output"]["GrinderClassPathExtensions"] = "*"

    def _setup_confluence(self, confluence_instance):
        SetupHelper().do_setup(conf_instance=confluence_instance)
        cluster_size = self._get_cluster_size(confluence_instance.properties)
        if cluster_size > 1:
            self._log.info("--------------------Scaling Confluence--------------------")
            self._scale_confluence_instances(self._stack_config["StackName"], cluster_size)

    def _scale_confluence_instances(self, stack_name, expected_nodes):
        # Update scaling group of Confluence
        csg_name = self.get_stack_output(stack_name, 'ClusterNodeGroup')
        expected_nodes_count = int(expected_nodes)
        self._aws.auto_scaling.update_auto_scaling_group(
            AutoScalingGroupName=csg_name,
            MinSize=expected_nodes_count,
            MaxSize=expected_nodes_count,
            DesiredCapacity=expected_nodes_count)

        self._wait_until_instances_ready_to_serve(stack_name, expected_nodes_count, 900)

    def _wait_until_instances_ready_to_serve(self, stack_name, expected_instances_count, max_poll_time_seconds):
        log = logging.getLogger("waiting for Confluence nodes ready to serve")
        start_time = Utils.now()
        alb_target_group_name = self.get_stack_output(stack_name, 'ConfluenceTargetGroupARN')
        while True:
            try:
                ready_instances_count = self._get_ready_instances_count(alb_target_group_name)
                log.debug("Number of instances ready: %s", ready_instances_count)
                if expected_instances_count == ready_instances_count:
                    self._log.info("All instances are ready to serve")
                    return True

                time.sleep(15)
            finally:
                if (Utils.now() - start_time).seconds > max_poll_time_seconds:
                    log.warn("Timed out polling for response from Confluence load balancer %s", alb_target_group_name)
                    return False

    def _get_ready_instances_count(self, alb_target_group_name):
        elb = self._aws.elb
        ready_instances_count = 0
        alb_instance_health_response = elb.describe_target_health(TargetGroupArn=alb_target_group_name)
        for target in alb_instance_health_response["TargetHealthDescriptions"]:
            if "healthy" in target["TargetHealth"]["State"]:
                ready_instances_count += 1

        return ready_instances_count

    def before_provision(self):
        # should check if we have a key pair already
        self.create_key_pair()

    def start_synchrony(self, stack_name):
        # Update scaling group of Synchrony
        ssg_name = self.get_stack_output(stack_name, 'SynchronyClusterNodeGroup')
        self._aws.auto_scaling.update_auto_scaling_group(
            AutoScalingGroupName=ssg_name,
            MinSize=1,
            MaxSize=1,
            DesiredCapacity=1)
        self.wait_synchrony_start(stack_name)
        self.turn_on_synchrony(stack_name)

    def write_provisioning_metadata(self):
        # write provision information into properties file.
        # Will easier to to export provisioning metadata into bamboo variables
        try:
            filename = os.path.join(e3.get_e3_home(), "instances", "confluence-provision.properties")
            if not os.path.exists(os.path.dirname(filename)):
                os.makedirs(os.path.dirname(filename))
            fo = open(filename, "wb")
            for item in self._stack_config["Output"]:
                fo.write("%s=%s\n" % (item, self._stack_config["Output"][item]))
        except IOError as e:
            self._log.error("Could not write confluence-provision.properties: %s" % e)
        else:
            fo.close()

    def wait_confluence_start(self, stack_name):
        status_url = self.get_stack_output(stack_name, 'URL') + '/status'
        return Utils.poll_url(
            status_url,
            900,
            lambda response: response.text == '{"state":"RUNNING"}' or response.text == '{"state":"FIRST_RUN"}'
        )

    def turn_on_synchrony(self, stack_name):
        enable_url = self.get_stack_output(stack_name, 'URL') + '/rest/synchrony-interop/enable'
        self._log.info("Enabling synchrony: %s " % enable_url)
        headers = {'Content-Type': 'application/json', 'X-Atlassian-Token': 'no-check'}
        requests.post(enable_url, auth=('admin', 'admin'), headers=headers)
        status_url = self.get_stack_output(stack_name, 'URL') + '/rest/synchrony-interop/status'
        return Utils.poll_url(
            status_url,
            900,
            lambda response: "\"synchronyEnabled\":true" in response.text and
                             "\"sharedDraftsEnabled\":true" in response.text,
            auth=('admin', 'admin')
        )

    def wait_synchrony_start(self, stack_name):
        status_url = self.get_stack_output(stack_name, 'URL') + '/synchrony/heartbeat'
        return Utils.poll_url(
            status_url,
            900,
            lambda response: "OK" in response.text
        )
