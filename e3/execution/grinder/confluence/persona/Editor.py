from confluence.common.helper.ConfluenceUserCreator import *
from confluence.common.helper.Authentication import login, logout
from confluence.common.helper.Utils import *
from confluence.common.helper.SynchronyUtils import *
from confluence.common.CommonActions import *
from confluence.common.wrapper.User import User

from Tools import *
from TestScript import TestScript

from net.grinder.script.Grinder import grinder
from synchrony.loadtest.java.interaction import BasicInteraction

import random


class Editor(TestScript):
    EDITING_SESSION_DURATION = 10000L

    def __init__(self, number, args):
        super(Editor, self).__init__(number, args)
        self._current_user = None

        self._space_key = url_encode(get_space_key())
        self._pages_to_edit = get_pages_to_edit()

    def __call__(self, *args, **kwargs):
        self._user_name = get_user_name()
        self._current_user = User(self._user_name, self._user_name)

        grinder.logger.info("Starting Editor persona with user name %s" % self._user_name)
        safe_execute(self, self.logger, self._test, self.report_success)

    def _test(self):
        """
        Simulate editor who edit a page. Macro will randomly add by on it weight
        :return: True or False according to test success or not
        """
        if self._current_user is None:
            return False

        login(self, self._current_user)

        visit_dashboard(self)

        run_num = grinder.getRunNumber()

        # view page
        item_num = random.randint(0, len(self._pages_to_edit) - 1)
        page_response = view_page_by_title(self, self._pages_to_edit[item_num]["page"], self._space_key)
        page_id = get_meta_attribute(page_response, 'ajs-page-id')  # pageId
        space_key = get_meta_attribute(page_response, 'ajs-space-key')
        page_xsrf_token = get_meta_attribute(page_response, 'ajs-atl-token')

        if is_running_special_action(self, run_num):
            grinder.logger.info("Upload image and visit page history")
            upload_image_into_page(self, page_id, space_key, page_xsrf_token)
            visit_page_attachments(self, page_id)
            visit_page_history(self, page_id)

        # edit page
        editor_response = self.edit_page(page_id)

        parent_page_id = get_meta_attribute(editor_response, 'ajs-parent-page-id')  # parentPageString
        page_title = get_meta_attribute(editor_response, 'ajs-page-title')  # pageTitle
        space_key = get_meta_attribute(editor_response, 'ajs-space-key')  # newSpaceKey
        page_source_label = 'default_page_property'
        source_page_title = 'Default Page Property and Excerpt'

        # ########### Getting synchrony data ###########
        synchrony_jwt_token = get_meta_attribute(editor_response, 'ajs-synchrony-token')
        synchrony_application_id = get_meta_attribute(editor_response, 'ajs-synchrony-app-id')
        synchrony_base_url = get_meta_attribute(editor_response, 'ajs-synchrony-base-url')
        if not validate_synchrony_meta(synchrony_jwt_token, page_id, synchrony_application_id,
                                       synchrony_base_url):
            grinder.logger.error("Synchrony metadata is not provided: token=%s content_id=%s application_id=%s "
                                 "synchrony_base_url=%s" %
                                 (synchrony_jwt_token, page_id, synchrony_application_id, synchrony_base_url))
            self.report_success(False)
            return

        # ########### Getting draft data ###########
        draft_type = get_meta_attribute(editor_response, 'ajs-draft-type')
        sync_rev = get_input_value(editor_response, 'syncRev')
        draft_id = get_meta_attribute(editor_response, 'ajs-draft-id')
        if not validate_drafts_data(draft_id, draft_type, sync_rev):
            grinder.logger.error("Drafts metadata is not provided: draft_id=%s draft_type=%s "
                                 "sync_rev=%s" % (draft_id, draft_type, sync_rev))
            self.report_success(False)
            return

        # ########### Editing page ###########
        synchrony_session = BasicInteraction()
        synchrony_options = create_synchrony_options(synchrony_jwt_token, page_id,
                                                     synchrony_application_id,
                                                     parse_synchrony_url(synchrony_base_url))

        # Page editing
        synchrony_session.run(synchrony_options, self.EDITING_SESSION_DURATION)
        edit_page_body = "Edited by %s:    " % self._user_name
        editor_save_draft(self, draft_id, page_id, edit_page_body, page_title, space_key, sync_rev)

        # Page editing - another session
        synchrony_session.run(synchrony_options, self.EDITING_SESSION_DURATION)
        # ####################################

        # submit page
        random_macros_result = get_random_macro_editor_format(
            self,
            page_id,
            defaultdict(str, report_label=page_source_label,
                        source_page_title=source_page_title),
            macro_browser_callback
        )
        edit_page_body += random_macros_result['body']
        editor_save_draft(self, draft_id, page_id, edit_page_body, page_title, space_key)
        editor_publish_page(self, parent_page_id,
                            edit_page_body,
                            page_id,
                            space_key,
                            page_title,
                            sync_rev,
                            2)

        is_success = is_http_ok()

        logout(self)

        return is_success

    def edit_page(self, page_id):
        return self.http("GET", "/pages/editpage.action", {"pageId": page_id})
